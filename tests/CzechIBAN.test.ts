import {CzechIBAN} from "../src/CzechIBAN";

describe('testing CzechIBAN', () => {
    const validCzechIBANsDataset: Array<[string]> = [
        ['CZ5508000000001234567899'],
        ['CZ55 0800 0000 0012 3456 7899'],
    ];

    test.each(validCzechIBANsDataset)('Valid Czech IBAN constructs', (ibanString: string) => {
        expect(String(new CzechIBAN(ibanString))).toBe('CZ5508000000001234567899');
    });

    const invalidCzechIBANs: Array<[string]> = [
        ['DE5508000000001234567899'], // not Czech
        ['CZ4508000000001234567899'], // wrong checksum1
        ['CZ5408000000001234567899'], // wrong checksum2
        ['CZ3008010000001234567899'], // unknown bankCode
        ['CZ0708000000001234567890'], // wrong Czech BBAN
    ];

    test.each(invalidCzechIBANs)('Invalid Czech IBAN fails to construct', (invalidIBANString: string) => {
        expect(() => { new CzechIBAN(invalidIBANString); }).toThrowError('Invalid Czech IBAN!');
    });

    test('Getters work', () => {
        const iban: CzechIBAN = new CzechIBAN('CZ8808000000190123456788');

        expect(iban.IBAN).toBe('CZ8808000000190123456788');
        expect(iban.bankCode).toBe('0800');
        expect(iban.accountPrefix).toBe('19');
        expect(iban.accountSuffix).toBe('123456788');
        expect(iban.BIC).toBe('GIBACZPX');
        expect(iban.bankName).toContain('Česká spořitelna');
    });

    test('Formatting as national string works', () => {
        const iban: CzechIBAN = new CzechIBAN('CZ8808000000190123456788');

        expect(iban.toNationalString()).toBe('19-123456788/0800');
    });

    const validCzechNationalAccountNumbers: Array<[string, string]> = [
        ['19-123456788/0800', 'CZ8808000000190123456788'],
        ['123456788/0800', 'CZ0508000000000123456788'],
        ['123456788/0300', 'CZ8903000000000123456788'],
        ['51/0100', 'CZ8201000000000000000051'],
        ['999993-9999999999/0800', 'CZ4708009999939999999999'],
    ];

    test.each(validCzechNationalAccountNumbers)(
        'Parsing national string works',
        (validNationalAccountNumber: string, resultingIBAN: string) => {
            const iban: CzechIBAN = CzechIBAN.fromNationalString(validNationalAccountNumber);

            expect(iban.IBAN).toBe(resultingIBAN);
        }
    );

    const invalidCzechNationalAccountNumbers: Array<[string]> = [
        ['-123456788/0800'],
        ['19-/0800'],
        ['19-123456788'],
        ['0800'],
        ['/0800'],
        ['0000000000/0800'],
        ['0000000001/0800'],
        ['0000000011/0800'],
        ['0123456790/0800'],
        ['123456788/0801'],
        ['123456788/080'],
        ['00123456788/0800'],
        ['0000000-123456788/0800'],
    ];

    test.each(invalidCzechNationalAccountNumbers)(
        'Invalid national account number throws',
        (invalidNationalAccountNumber: string) => {
            expect(() => {CzechIBAN.fromNationalString(invalidNationalAccountNumber)}).toThrowError();
        });
})
