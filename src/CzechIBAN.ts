// noinspection JSUnusedGlobalSymbols

import { composeIBAN, extractIBAN, ExtractIBANResult, validateIBAN } from 'ibantools';

type BankInformation = {
  code: string;
  name: string;
  BIC?: string;
};

const banksInformation: BankInformation[] = [
  { code: '0100', name: 'Komerční banka, a.s.', BIC: 'KOMBCZPP' },
  { code: '0300', name: 'Československá obchodní banka, a.s.', BIC: 'CEKOCZPP' },
  { code: '0600', name: 'MONETA Money Bank, a.s.', BIC: 'AGBACZPP' },
  { code: '0710', name: 'Česká národní banka', BIC: 'CNBACZPP' },
  { code: '0800', name: 'Česká spořitelna, a.s.', BIC: 'GIBACZPX' },
  { code: '2010', name: 'Fio banka, a.s.', BIC: 'FIOBCZPP' },
  { code: '2020', name: 'MUFG Bank (Europe) N.V. Prague Branch', BIC: 'BOTKCZPP' },
  { code: '2060', name: 'Citfin, spořitelní družstvo', BIC: 'CITFCZPP' },
  { code: '2070', name: 'TRINITY BANK a.s.', BIC: 'MPUBCZPP' },
  { code: '2100', name: 'Hypoteční banka, a.s.' },
  { code: '2200', name: 'Peněžní dům, spořitelní družstvo' },
  { code: '2220', name: 'Artesa, spořitelní družstvo', BIC: 'ARTTCZPP' },
  { code: '2250', name: 'Banka CREDITAS a.s.', BIC: 'CTASCZ22' },
  { code: '2260', name: 'NEY spořitelní družstvo' },
  { code: '2275', name: 'Podnikatelská družstevní záložna' },
  { code: '2600', name: 'Citibank Europe plc, organizační složka', BIC: 'CITICZPX' },
  { code: '2700', name: 'UniCredit Bank Czech Republic and Slovakia, a.s.', BIC: 'BACXCZPP' },
  { code: '3030', name: 'Air Bank a.s.', BIC: 'AIRACZPP' },
  { code: '3050', name: 'BNP Paribas Personal Finance SA, odštěpný závod', BIC: 'BPPFCZP1' },
  { code: '3060', name: 'PKO BP S.A., Czech Branch', BIC: 'BPKOCZPP' },
  { code: '3500', name: 'ING Bank N.V.', BIC: 'INGBCZPP' },
  { code: '4000', name: 'Max banka a.s. (dříve Expobank CZ a.s.)', BIC: 'EXPNCZPP' },
  { code: '4300', name: 'Národní rozvojová banka, a.s.', BIC: 'CMZRCZP1' },
  { code: '5500', name: 'Raiffeisenbank a.s.', BIC: 'RZBCCZPP' },
  { code: '5800', name: 'J & T Banka, a.s.', BIC: 'JTBPCZPP' },
  { code: '6000', name: 'PPF banka a.s.', BIC: 'PMBPCZPP' },
  { code: '6100', name: 'Raiffeisenbank a.s. (do 31. 12. 2021 Equa bank a.s.)', BIC: 'EQBKCZPP' },
  { code: '6200', name: 'COMMERZBANK Aktiengesellschaft, pobočka Praha', BIC: 'COBACZPX' },
  { code: '6210', name: 'mBank S.A., organizační složka', BIC: 'BREXCZPP' },
  { code: '6300', name: 'BNP Paribas S.A., pobočka Česká republika', BIC: 'GEBACZPP' },
  { code: '6700', name: 'Všeobecná úverová banka a.s., pobočka Praha', BIC: 'SUBACZPP' },
  { code: '7910', name: 'Deutsche Bank Aktiengesellschaft Filiale Prag, organizační složka', BIC: 'DEUTCZPX' },
  { code: '7950', name: 'Raiffeisen stavební spořitelna a.s.' },
  { code: '7960', name: 'ČSOB Stavební spořitelna, a.s.' },
  { code: '7970', name: 'MONETA Stavební spořitelna, a.s.' },
  { code: '7990', name: 'Modrá pyramida stavební spořitelna, a.s.' },
  { code: '8030', name: 'Volksbank Raiffeisenbank Nordoberpfalz eG pobočka Cheb', BIC: 'GENOCZ21' },
  { code: '8040', name: 'Oberbank AG pobočka Česká republika', BIC: 'OBKLCZ2X' },
  { code: '8060', name: 'Stavební spořitelna České spořitelny, a.s.' },
  { code: '8090', name: 'Česká exportní banka, a.s.', BIC: 'CZEECZPP' },
  { code: '8150', name: 'HSBC Bank plc - pobočka Praha', BIC: 'MIDLCZPP' },
  { code: '8190', name: 'Sparkasse Oberlausitz-Niederschlesien' },
  { code: '8198', name: 'FAS finance company s.r.o.', BIC: 'FFCSCZP1' },
  { code: '8199', name: 'MoneyPolo Europe s.r.o.', BIC: 'MOUSCZP2' },
  {
    code: '8200',
    name: 'PRIVAT BANK der Raiffeisenlandesbank Oberösterreich Aktiengesellschaft, pobočka Česká republika',
  },
  { code: '8220', name: 'Payment Execution s.r.o.', BIC: 'PAERCZP1' },
  { code: '8230', name: 'ABAPAY s.r.o.' },
  { code: '8240', name: 'Družstevní záložna Kredit, v likvidaci' },
  { code: '8250', name: 'Bank of China (CEE) Ltd. Prague Branch', BIC: 'BKCHCZPP' },
  { code: '8255', name: 'Bank of Communications Co., Ltd., Prague Branch odštěpný závod', BIC: 'COMMCZPP' },
  {
    code: '8265',
    name: 'Industrial and Commercial Bank of China Limited, Prague Branch, odštěpný závod',
    BIC: 'ICBKCZPP',
  },
  { code: '8270', name: 'Fairplay Pay s.r.o.', BIC: 'FAPOCZP1' },
  { code: '8280', name: 'B-Efekt a.s.', BIC: 'BEFKCZP1' },
  { code: '8293', name: 'Mercurius partners s.r.o.', BIC: 'MRPSCZPP' },
  { code: '8250', name: 'BESTPAY s.r.o.', BIC: 'BEORCZP2' },
  { code: '8500', name: 'Ferratum Bank plc' },
];

export class CzechIBAN {
  public static IBANCountryCode = 'CZ';

  public readonly IBAN: string;

  constructor(IBAN: string) {
    this.IBAN = extractIBAN(IBAN).iban;

    if (!isCzechIBANString(this.IBAN)) {
      throw new Error('Invalid Czech IBAN!');
    }
  }

  public get bankCode(): string {
    return this.IBAN.substring(4, 8);
  }

  public get accountPrefix(): string {
    return this.IBAN.substring(8, 14).replace(/^0+/, '');
  }

  public get accountSuffix(): string {
    return this.IBAN.substring(14, 24).replace(/^0+/, '');
  }

  public get bankName(): string {
    return banksInformation.find((value: BankInformation): boolean => value.code === this.bankCode)!.name;
  }

  public get BIC(): string | undefined {
    return banksInformation.find((value: BankInformation): boolean => value.code === this.bankCode)!.BIC;
  }

  public static fromNationalString(input: string): CzechIBAN {
    if (!/^([0-9]{1,6}-)?[0-9]{2,10}\/[0-9]{4}/.test(input)) {
      throw new Error('Invalid account number string!');
    }

    const splitBankCode: string[] = input.split('/'),
      bankCode: string = splitBankCode[1],
      splitAccountNumber: string[] = splitBankCode[0].split('-'),
      [accountNumberPrefix, accountNumberSuffix]: [string, string] =
        splitAccountNumber.length === 1 ? ['', splitAccountNumber[0]] : [splitAccountNumber[0], splitAccountNumber[1]],
      IBANString: string | null = composeIBAN({
        countryCode: CzechIBAN.IBANCountryCode,
        bban: bankCode + accountNumberPrefix.padStart(6, '0') + accountNumberSuffix.padStart(10, '0'),
      });

    if (IBANString === null) {
      throw new Error('Invalid bank account national number!');
    }

    return new CzechIBAN(IBANString);
  }

  public toString(): string {
    return this.IBAN;
  }

  public toNationalString(): string {
    return `${this.accountPrefix === '' ? '' : `${this.accountPrefix}-`}${this.accountSuffix}/${this.bankCode}`;
  }
}

type BankCode = string & {
  readonly BankCode: unique symbol;
};

const isBankCode = (input: unknown): input is BankCode =>
  typeof input === 'string' &&
  banksInformation.find((value: BankInformation): boolean => input === value.code) !== undefined;

type AccountNumberSuffix = string & {
  readonly AccountNumberSuffix: unique symbol;
};

const isAccountNumberSuffix = (input: unknown): input is AccountNumberSuffix =>
  typeof input === 'string' && input.length <= 10 && input.replace(/0/g, '').length >= 2;

type CzechIBANString = string & {
  readonly CzechIBANString: unique symbol;
};

const isCzechIBANString = (input: unknown): input is CzechIBANString => {
  if (typeof input !== 'string') {
    return false;
  }

  const extractedIBAN: ExtractIBANResult = extractIBAN(input);

  return (
    extractedIBAN.valid &&
    validateIBAN(extractedIBAN.iban) &&
    extractedIBAN.countryCode === CzechIBAN.IBANCountryCode &&
    isBankCode(extractedIBAN.iban.substring(4, 8)) &&
    isAccountNumberSuffix(extractedIBAN.iban.substring(14, 24))
  );
};
